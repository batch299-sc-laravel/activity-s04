<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostController extends Controller
{
    // This function will run depending if the route matches this function.
    public function create(){
        // It will return the create.blade.php file from the views folder.
        return view('posts.create');
    }

    public function store(Request $request){
        // Checks if a user is logged in
        if(Auth::user()){  
            // 1. Instatiates a new instance of a post 
            $post = new Post;

            // 2. Assigns a value for each field in a post 
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = Auth::user()->id;

            // 3. Save the newly assigned data to the database
            $post->save();

            // 4. Redirect to the /posts endpoint with a GET method
            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    // public function index(){
    //     // Retrieves ALL of the posts from the 'posts' table
    //     $posts = Post::all();

    //     // Returns the posts.index view along with ALL of the posts from the 'posts' table
    //     return view('posts.index')->with('posts', $posts);
    // }

    public function index(){
    $posts = Post::where('isActive', true)->get();
    return view('posts.index')->with('posts', $posts);
    }

    // public function show($id){
    //     // The selected post's ID will be passed here and used in the find() function of the Post model to find the specific post with that ID.
    //     $post = Post::find($id);

    //     return view('posts.show')->with('post', $post);
    // }

    public function show($id){
    $post = Post::where('isActive', true)->find($id);

    return view('posts.show')->with('post', $post);
    }


    public function edit($id){
        $post = Post::find($id);

        return view('posts.edit')->with('post', $post);
    }

    public function update(Request $request, $id){
        //Get the existing post thru ID
        $post = Post::find($id);

        //Check if the logged in user is the author of the post
        if(Auth::user()->id == $post->user_id){
            $post->title =$request->input('title');
            $post->content = $request->input('content');

            $post->save();
        }

        return redirect('/posts');
    }

    // public function destroy($id){
    //     $post = Post::find($id);

    //     if(Auth::user()->id == $post->user_id){
    //         $post->delete();
    //     }
    //     return redirect ('/posts');
    // }

    public function archive($id){
    $post = Post::find($id);

    if (Auth::user()->id == $post->user_id) {
        $post->isActive = false;
        $post->save();
    }

    return redirect('/posts');
}


}
