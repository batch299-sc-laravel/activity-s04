<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        //Using the schema class, it utilizes the "create" function to create a database table.
        Schema::create('posts', function (Blueprint $table) {
            $table->id(); //Primary key that auto-increments
            $table->string('title');
            $table->text('content');
            $table->boolean('isActive')->default(true);
            $table->unsignedBigInteger('user_id'); //To be connected to another table as a foreign key
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
